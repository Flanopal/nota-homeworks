function getInfo()
	return {
		onNoUnits = SUCCESS,
		tooltip = "Some tip",
		parameterDefs = {
			{ 
				name = "position",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			},
			{ 
				name = "units",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			}
		}
	}
end


local MoveOrder = Spring.GiveOrderToUnit

local function ClearState(self)
end

-- @send units
function Run(self, units, parameter)
	local list = parameter.listOfUnits
	local pos = parameter.position
	
	local cmd = CMD.MOVE
	for i=1, #list do
		MoveOrder(list[i],cmd,pos,{})
	end
	return RUNNING
end

function Reset(self)
	ClearState(self)
end
