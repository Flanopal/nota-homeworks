function getInfo()
	return {
		onNoUnits = SUCCESS,
		tooltip = "Execute movement plan for unit",
		parameterDefs = {
			{ 
				name = "unit",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			},
			{ 
				name = "plan",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			}
		}
	}
end


local MoveOrder = Spring.GiveOrderToUnit

local function ClearState(self)
end

-- @execute plan
function Run(self, units, parameter)
	local unit = parameter.unit
	local plan = parameter.plan
	local index = plan[1]
	local position = Vec3(Spring.GetUnitPosition(unit))
	local move = CMD.MOVE

	if index == 1 or position:Distance(plan[index]) < math.max(math.abs(position.y - plan[index].y)*1.25,180) then
		-- we reached plan position, lets get to next one
		index = index + 1
		if index > #plan then
			-- plan is finished
			plan[1] = 1
			return SUCCESS
		end

		local point = plan[index]
		MoveOrder(unit,move,{point.x,point.y,point.z},{})
	end
	plan[1] = index
	return RUNNING
end

function Reset(self)
	ClearState(self)
end