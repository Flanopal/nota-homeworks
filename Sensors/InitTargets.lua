local sensorInfo = {
	name = "Init targets",
	desc = "Init targets",
	author = "Flanopal",
	date = "2019-05-12",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = -1 -- acutal, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

-- @Init targets
return function(targets, basePositions)
	
	targets["stateless"] = #units
	for i=1, #units do
		targets[#targets + 1] = {}
		targets[#targets]["unit"] = units[i]
		targets[#targets]["evacuator"] = nil
		targets[#targets]["evacPosition"] = basePositions[#basePositions]
		basePositions[#basePositions] = nil
	end
	return targets
end