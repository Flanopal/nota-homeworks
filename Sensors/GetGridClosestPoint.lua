local sensorInfo = {
	name = "Get closest grid point",
	desc = "Get closest grid point",
	author = "Flanopal",
	date = "2019-05-13",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = -1 -- acutal, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

-- speedups
local GetPosition = Spring.GetUnitPosition

-- @get closest grid point
return function(points,position)
	local point = nil
	local minDist = math.huge
	for i=1,#points do
		local dist = points[i]:Distance(position)
		if dist < minDist then
			minDist = dist
			point = points[i]
		end
	end
	return point
end