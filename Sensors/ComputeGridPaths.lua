local sensorInfo = {
	name = "Precompute paths in grid",
	desc = "Precompute paths in grid",
	author = "Flanopal",
	date = "2019-05-13",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = -1 -- acutal, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

-- speedups
local GetPosition = Spring.GetUnitPosition

-- @fill grid paths via BFS
return function(grid)
	local frontier = {}
	local frontier2 = {}
	local gridExtractPoints = {}

	-- fill frontier with extract points, paths are planned to these points
	for i = 1,  #grid["extractPoints"] do
		local gridExtractPoint = Sensors.nota_zavorka_hw.GetGridClosestPoint(grid["points"], grid["extractPoints"][i])
		frontier[#frontier + 1] = gridExtractPoint
		grid[gridExtractPoint]["distance"] = 0
		gridExtractPoints[#gridExtractPoints + 1] = gridExtractPoint
	end

	while #frontier > 0 do
		local neighs = grid[frontier[#frontier]]["neighbours"]
		local distance = grid[frontier[#frontier]]["distance"]
		for i=1,#neighs do
			local dif = neighs[i]:Distance(frontier[#frontier])
			local assign = grid[neighs[i]]["previous"] == nil 

			-- if we already have previous we want to choose better
			if assign == false then
				if grid[neighs[i]]["previous"].y > frontier[#frontier].y + 10 then				
					-- better are lower points, but the distance grow must't be huge
					if grid[neighs[i]]["distance"] > distance + dif - 200 then
						assign = true
					end
				elseif grid[neighs[i]]["previous"].y > frontier[#frontier].y - 10 then
					-- if the height is nearly the same, distance is point of interest
					if grid[neighs[i]]["distance"] > distance + dif then
						assign = true
					end
				elseif grid[neighs[i]]["distance"] > distance + dif + 200 then
					assign = true
				end
			end
			
			if assign then
				grid[neighs[i]]["previous"] = frontier[#frontier]
				grid[neighs[i]]["distance"] = distance + dif
				frontier2[#frontier2 + 1] = neighs[i]
			end

		end

		frontier[#frontier] = nil
		if #frontier == 0 then
			frontier = frontier2
			frontier2 = {}
		end
	end

	-- grid extrack points must't have ancestor to stop backtrack
	for i = 1,#gridExtractPoints do
		grid[gridExtractPoints[i]]["previous"] = nil
	end

	return grid
end