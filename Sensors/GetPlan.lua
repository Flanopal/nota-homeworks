local sensorInfo = {
	name = "Get plan to target unit",
	desc = "Get plan to target unit",
	author = "Flanopal",
	date = "2019-05-12",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = -1 -- acutal, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

-- @get plan
return function(target,grid)
	local targetPos = Vec3(Spring.GetUnitPosition(target))
	local gridPoint = Sensors.nota_zavorka_hw.GetGridClosestPoint(grid["points"],targetPos)
	-- start plan with absolute final position
	-- first number in plan is index used during plan execution
	local plan = {1,targetPos,gridPoint}
	local point = grid[gridPoint]

	-- backtract in grid to reach grid evacuation point
	while point["previous"] ~= nil do
		plan[#plan + 1] = point["previous"]
		point = grid[point["previous"]]
	end

	-- we need to reverse plan to obtain path from evacuation point to target
	return Sensors.nota_zavorka_hw.ReversePlan(plan)
end