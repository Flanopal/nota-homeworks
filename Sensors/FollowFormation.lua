local sensorInfo = {
	name = "Send units to positions",
	desc = "Set units to positions",
	author = "Flanopal",
	date = "2018-05-13",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = 0 -- acutal, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end
local MoveOrder = Spring.GiveOrderToUnit

-- @send units
return function(point,dx,dz)
	local i=1
	local j=1
	while i<=#units do
		local target = Vec3(point.x,point.y,point.z)
		if j==2 then
			target.x=target.x+dz
			target.z=target.z-dx
		elseif j==3 then
			target.x=target.x-dz
			target.z=target.z+dx
		end
		MoveOrder(units[i], CMD.MOVE, {target.x,target.y,target.z}, {})
		i=i+1
		j=j+1
		if j==4 then
			point.x=point.x-dx
			point.z=point.z-dz
			j=1
		end
	end
end