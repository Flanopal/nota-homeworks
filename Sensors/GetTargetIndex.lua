local sensorInfo = {
	name = "Get target for atlas",
	desc = "Get target for atlas",
	author = "Flanopal",
	date = "2019-05-12",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = -1 -- acutal, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

-- @assign target
return function(atlas,targets)
	local target = {}

	-- first find target without evacuator
	for i=1, #targets do
		-- if there is a state, unit is evacuated or dead
		if targets[i]["state"] == nil then
			local dead = Spring.GetUnitIsDead(targets[i]["unit"])
			if dead == true or dead == nil then
				targets[i]["state"] = "dead"
			elseif targets[i]["evacuator"] == nil then
				-- unit has none evacuator -> lets save it
				targets[i]["evacuator"] = atlas
				return	i
			end	
		end
	end
	
	-- check targets with evacuator
	local stateless = 0
	for i=1, #targets do
		if targets[i]["state"] == nil then
			stateless = stateless + 1
			local dead = Spring.GetUnitIsDead(targets[i]["evacuator"])
			if dead == true or dead == nil then
				-- if the evacuator is dead we can replace him
				targets[i]["evacuator"] = atlas
				return i
			end
		end
	end
	targets["stateless"] = stateless
	return 0
end